from filme import Filme
from serie import Serie
from playlist import Playlist
import random
from abc import ABC

vingadores = Filme('Vingadores - gUErra Infina', 2019, 160)
atlanta = Serie('Atlanta',  2018, 2)
tmep = Filme('Todo mundo em pânico', 1999, 100)
demolidor = Serie('Demolidor', 2016, 2)

filmes_e_series = [vingadores, atlanta, demolidor, tmep]

for x in range(0, random.randrange(0, 101)):
    filmes_e_series[random.randrange(0, len(filmes_e_series))].dar_like()


playlist = Playlist("Fim de semana", filmes_e_series)

for programa in playlist:
    print(programa)

print(f'\n\nTamanho de \'{playlist.nome}\': {len(playlist)}')
print (f'Ta ou não tá? {demolidor in playlist}')