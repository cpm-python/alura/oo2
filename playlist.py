class Playlist:
    def __init__(self, nome, programas):
        self._nome = nome.title()
        self._programas = programas

    def __getitem__(self, item):
        return self._programas[item]

    @property
    def nome(self):
        return self._nome

    @property
    def listagem(self):
        return self._programas

    def __len__(self):
        return len(self._programas)